/**
 * An enum of the area that have convert ability.
 * @author Arut Thanomwatana
 *
 */
public enum Area implements Unit 
{
	SQUARE_METER("Square meter",1.0),
	SQUARE_KILOMETER("Square km",1000000.0),
	SQUARE_CENTEMETER("Square cm",0.0001),
	SQUARE_MILE("Square mile",25889957.2110),
	SQUARE_FOOT("Square foot",0.9290),
	RAI("Rai",1600.0);
	
	/**Name of the Unit. */
	public final String NAME;
	/** Value in one Unit. */
	public final double VAL;
	
	/**
	 * Initialize enum Area
	 * @param name is a name of the unit.
	 * @param val is a value in 1 unit.
	 */
	
	private Area(String name,double val)
	{
		this.NAME = name;
		this.VAL = val;
	}
	
	/**
	 * Define the Unit.
	 * @return name of the Unit
	 */
	public String toString()
	{
		return this.NAME;
	}
	
	/**
	 * Convert value form 1 unit to another unit.
	 * @param amt is a amount that want to be converted
	 * @param unit is a unit that will be converted to
	 */
	public double convertTo(double amt,Unit unit)
	{
		return this.VAL*amt/unit.getValue();
	}

	/**
	 * Get the value of the Unit.
	 * @return Value of the Unit
	 */
	public double getValue() 
	{
		return this.VAL;
	}

}
