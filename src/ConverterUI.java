import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Arrays;
/**
 * Converter Graphical User Interface.
 * @author Arut Thanomwatana
 *
 */
public class ConverterUI extends JFrame
{
	// attributes for graphical components
	private JMenuBar menuBar;
	private JMenu menu;
	private JButton convertButton;
	private JButton clearButton;
	private JTextField inputField1;
	private JTextField inputField2;
	private JLabel equals;
	private JComboBox unit1ComboBox;
	private JComboBox unit2ComboBox;
	private UnitConverter unitconverter;
	private int sided;


	/**
	 * Initialize the Converter UI.
	 * @param uc is the UnitConverter
	 */
	public ConverterUI( UnitConverter uc ) {
		this.unitconverter = uc;
		this.setTitle("Length Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		sided = 1;
		initComponents( );
		this.setVisible(true);
	}

	/**
	 * Initialize components in the window.
	 */
	private void initComponents() {

		menuBar = new JMenuBar();
		menu = new JMenu("Unit Type");
		menu.add(new ChangeTypeAction("Length"));
		menu.add(new ChangeTypeAction("Weight"));
		menu.add(new ChangeTypeAction("Area"));
		menu.add(new ChangeTypeAction("Volume"));
		menu.addSeparator();
		menu.add(new ExitAction());


		inputField1 = new JTextField(5);
		inputField2 = new JTextField(5);
		//inputField2.setEditable(false);
		menuBar.add(menu);

		equals = new JLabel("=");

		unit1ComboBox = new JComboBox<Unit>();
		Unit [] lengths = unitconverter.getUnits(UnitContainer.valueOf("LENGTH"));
		for(Unit u : lengths) unit1ComboBox.addItem(u);

		unit2ComboBox = new JComboBox<Unit>(lengths);

		clearButton = new JButton("Clear");

		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout( );
		contents.setLayout( layout );
		convertButton = new JButton("Convert");

		super.setJMenuBar(menuBar);
		contents.add(inputField1);
		contents.add(unit1ComboBox);
		contents.add(equals);
		contents.add(inputField2);
		contents.add(unit2ComboBox);
		contents.add(convertButton);
		contents.add(clearButton);

		ActionListener convert = new ConvertButtonListener( );
		ActionListener clear = new ClearButtonListener();

		convertButton.addActionListener( convert );
		inputField1.addActionListener(convert);
		unit1ComboBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				sided = 1;	
				System.out.println(sided);
			}
		});
		unit2ComboBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				sided = 2;	
				System.out.println(sided);
			}
		});
		clearButton.addActionListener(clear);

		this.pack(); 
	}

	/** Convert the value in textfield1 and sent the converted 
	 *  value to the textfield2 
	 */
	class ConvertButtonListener implements ActionListener {

		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			String s = inputField1.getText().trim();

			if ( s.length() > 0 ) {
				try{
					if(sided==1){
						inputField1.setForeground(Color.BLACK);
						inputField2.setForeground(Color.BLACK);
						double value = Double.valueOf( s );
						double result = unitconverter.convert(value,(Unit)unit1ComboBox.getSelectedItem(),(Unit)unit2ComboBox.getSelectedItem());
						String set = String.format("%.5g", result);
						inputField2.setText(set);
						
					}
					else{
						inputField2.setForeground(Color.BLACK);
						inputField1.setForeground(Color.BLACK);
						double value = Double.valueOf( s );
						double result = unitconverter.convert(value,(Unit)unit2ComboBox.getSelectedItem(),(Unit)unit1ComboBox.getSelectedItem());
						String set = String.format("%.5g", result);
						inputField1.setText(set);
						
					}
				} catch(NumberFormatException e){
					if(sided==1){
						inputField1.setForeground(Color.red);
						inputField2.setText("");
					}
					else{
						inputField2.setForeground(Color.RED);
						inputField1.setText("");
					}
				}
			}
		}
	} 
	/** Clear the text field into its default. */
	class ClearButtonListener implements ActionListener{

		public void actionPerformed(ActionEvent evt){
			inputField1.setText("");
			inputField1.setBackground(Color.WHITE);
			inputField2.setText("");
		}
	}
	/** Exit the Application */
	class ExitAction extends AbstractAction{
		public ExitAction(){
			super("Exit");
		}
		public void actionPerformed(ActionEvent evt){
			System.exit(0);
		}
	}
	/** Change Type of unit */
	class ChangeTypeAction extends AbstractAction{
		public ChangeTypeAction(String name){
			super(name);
		}
		public void actionPerformed(ActionEvent evt){
			UnitContainer type =  UnitContainer.valueOf(((JMenuItem)evt.getSource()).getText().toUpperCase());
			Unit [] unitType = unitconverter.getUnits(type);
			unit1ComboBox.removeAllItems();
			unit2ComboBox.removeAllItems();
			for(Unit u : unitType){
				unit1ComboBox.addItem(u);
				unit2ComboBox.addItem(u);
			}
		}

	}


	


}





