/**
 * An Unit container that contain all unit.
 * @author Arut Thanomwatana
 *
 */
public enum UnitContainer 
{
	LENGTH(Length.values()),
	AREA(Area.values()),
	WEIGHT(Weight.values()),
	VOLUME(Volume.values());
	
	/** Array of value of units. */
	private Unit [] unit;
	
	/**
	 * Initialize the Unit container
	 * @param u is a value of the unit.
	 */
	private UnitContainer(Unit [] u){
		this.unit = u;
	}
	
	/**
	 * Get the value of the unit.
	 * @return value of the unit
	 */
	public Unit [] getUnit()
	{
		return this.unit;
	}
}
