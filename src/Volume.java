/**
 * An enum of the volume that have convert ability.
 * @author Arut Thanomwatana
 *
 */
public enum Volume implements Unit
{
	CUBIC_METER("Cubic meter",1.0),
	LITRE("Litre",0.001),
	CUBIC_FOOT("Cubic foot",0.0283168),
	CUBIC_INCH("Cubic inch",1.5387E-5),
	MILLITER("Milliter",1E-6),
	Thang("Thang",0.02);
	
	/**Name of the Unit. */
	public final String NAME;
	/** Value in one Unit. */
	public final double VAL;
	
	/**
	 * Initialize enum Volume
	 * @param name is a name of the unit.
	 * @param val is a value in 1 unit.
	 */
	
	private Volume(String name,double val)
	{
		this.NAME = name;
		this.VAL = val;
	}
	
	/**
	 * Define the Unit.
	 * @return name of the Unit
	 */
	public String toString()
	{
		return this.NAME;
	}
	
	/**
	 * Convert value form 1 unit to another unit.
	 * @param amt is a amount that want to be converted
	 * @param unit is a unit that will be converted to
	 */
	public double convertTo(double amt,Unit unit)
	{
		return this.VAL*amt/unit.getValue();
	}

	/**
	 * Get the value of the Unit.
	 * @return Value of the Unit
	 */
	public double getValue() 
	{
		return this.VAL;
	}

}
