/**
 * An enum of the weight that have convert ability.
 * @author Arut Thanomwatana
 *
 */

public enum Weight implements Unit
{
	KILOGRAM("Kg",1.0),
	GRAM("Gram",0.001),
	OUNCE("Ounce",0.028),
	POUND("lb",0.453),
	Keed("Keed",0.1),
	Ton("Ton",1000.0);
	
	/**Name of the Unit. */
	public final String NAME;
	/** Value in one Unit. */
	public final double VAL;
	
	/**
	 * Initialize enum Weight.
	 * @param name is a name of the unit.
	 * @param val is a value in 1 unit.
	 */
	private Weight(String name,double val)
	{
		this.NAME = name;
		this.VAL = val;
	}
	
	/**
	 * Define the Unit.
	 * @return name of the Unit
	 */
	
	public String toString()
	{
		return this.NAME;
	}
	
	/**
	 * Convert value form 1 unit to another unit.
	 * @param amt is a amount that want to be converted
	 * @param unit is a unit that will be converted to
	 */
	
	public double convertTo(double amt,Unit unit)
	{
		return this.VAL*amt/unit.getValue();
	}
	
	/**
	 * Get the value of the Unit.
	 * @return Value of the Unit
	 */
	
	public double getValue()
	{
		return this.VAL;
	}


}
